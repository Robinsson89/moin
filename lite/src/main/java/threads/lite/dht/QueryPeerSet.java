package threads.lite.dht;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;


public class QueryPeerSet extends ConcurrentSkipListSet<QueryPeer> {

    public QueryPeerSet() {
        super(QueryPeer::compareTo);
    }

    public boolean isDone() {
        for (QueryPeer queryPeer : this) {
            PeerState state = queryPeer.getState();
            if (state == PeerState.PeerHeard || state == PeerState.PeerWaiting) {
                return false;
            }
        }
        return true;
    }

    public List<QueryPeer> nextHeardPeers(int maxLength) {
        // The peers we query next should be ones that we have only Heard about.
        List<QueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (QueryPeer queryPeer : this) {
            if (queryPeer.getState() == PeerState.PeerHeard) {
                peers.add(queryPeer);
                count++;
                if (count == maxLength) {
                    break;
                }
            }
        }
        return peers;

    }

}
