package threads.lite.core;

import androidx.annotation.NonNull;

import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public interface Connection {
    String REMOTE_PEER = "REMOTE_PEER";

    void close();

    boolean isConnected();

    @NonNull
    InetSocketAddress getRemoteAddress();

    @NonNull
    InetSocketAddress getLocalAddress();

    @NonNull
    PeerId remotePeerId();

    @NonNull
    CompletableFuture<Stream> createStream(@NonNull StreamHandler streamHandler);

    @NonNull
    Multiaddr remoteMultiaddr();

    int getSmoothedRtt();


}
