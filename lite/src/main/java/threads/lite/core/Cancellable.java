package threads.lite.core;


import threads.lite.IPFS;

public interface Cancellable {
    boolean isCancelled();

    default long timeout() {
        return IPFS.MAX_TIMEOUT;
    }
}
