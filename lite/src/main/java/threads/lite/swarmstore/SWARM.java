package threads.lite.swarmstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Multiaddr;
import threads.lite.core.SwarmStore;


public class SWARM implements SwarmStore {

    private static volatile SWARM INSTANCE = null;
    private final SwarmDatabase swarmDatabase;

    private SWARM(SwarmDatabase swarmDatabase) {
        this.swarmDatabase = swarmDatabase;
    }

    public static SWARM getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (SWARM.class) {
                if (INSTANCE == null) {
                    SwarmDatabase blocksDatabase = Room.databaseBuilder(context, SwarmDatabase.class,
                                    SwarmDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();


                    INSTANCE = new SWARM(blocksDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void storeMultiaddr(@NonNull Multiaddr multiaddr) {
        swarmDatabase.swarmDao().insertMultiaddr(multiaddr);
    }

    public void removeMultiaddr(@NonNull Multiaddr multiaddr) {
        swarmDatabase.swarmDao().removeMultiaddr(multiaddr);
    }


    @NonNull
    public List<Multiaddr> getMultiaddrs() {
        return swarmDatabase.swarmDao().getMultiaddrs();
    }

}
