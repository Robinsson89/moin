package threads.lite.cert;

import java.math.BigInteger;
import java.security.PublicKey;
import java.util.Date;
import java.util.Locale;

import threads.lite.asn1.x500.X500Name;
import threads.lite.asn1.x509.SubjectPublicKeyInfo;

/**
 * JCA helper class to allow JCA objects to be used in the construction of a Version 3 certificate.
 */
public class JcaX509v3CertificateBuilder extends X509v3CertificateBuilder {

    public JcaX509v3CertificateBuilder(X500Name issuer, BigInteger serial, Date notBefore,
                                       Date notAfter, Locale locale, X500Name subject,
                                       PublicKey publicKey) {
        super(issuer, serial, notBefore, notAfter, locale, subject,
                SubjectPublicKeyInfo.getInstance(publicKey.getEncoded()));
    }

}
