package threads.lite.utils;

import androidx.annotation.NonNull;

import threads.lite.core.Cancellable;

public class TimeoutCancellable extends TimeoutProgress {


    public TimeoutCancellable(long timeout) {
        super(timeout);
    }

    public TimeoutCancellable(@NonNull Cancellable cancellable, long timeout) {
        super(cancellable, timeout);
    }

    @Override
    public void setProgress(int progress) {
        throw new IllegalStateException("should not be invoked");
    }

    @Override
    public boolean doProgress() {
        return false;
    }
}
