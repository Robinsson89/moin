package threads.lite.mplex;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import threads.lite.core.Stream;
import threads.lite.core.Transport;
import threads.lite.noise.CipherState;

public class MuxedTransport implements Transport {
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;
    @NonNull
    private final QuicStream stream;

    public MuxedTransport(@NonNull QuicStream stream, @NonNull CipherState sender,
                          @NonNull CipherState receiver) {
        this.stream = stream;
        this.sender = sender;
        this.receiver = receiver;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @Override
    public Type getType() {
        return Type.MUXED;
    }

    @NonNull
    public QuicStream getQuicStream() {
        return stream;
    }

    @Override
    public Stream getStream() {
        return new MuxedStream(stream, getSender(), getReceiver());
    }
}
