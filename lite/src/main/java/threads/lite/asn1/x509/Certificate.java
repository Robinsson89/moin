package threads.lite.asn1.x509;

import java.util.Objects;

import threads.lite.asn1.ASN1BitString;
import threads.lite.asn1.ASN1Object;
import threads.lite.asn1.ASN1Primitive;
import threads.lite.asn1.ASN1Sequence;

/**
 * an X509Certificate structure.
 * <pre>
 *  Certificate ::= SEQUENCE {
 *      tbsCertificate          TBSCertificate,
 *      signatureAlgorithm      AlgorithmIdentifier,
 *      signature               BIT STRING
 *  }
 * </pre>
 */
public class Certificate extends ASN1Object {
    final ASN1Sequence seq;

    private Certificate(ASN1Sequence seq) {
        this.seq = seq;

        //
        // correct x509 certficate
        //
        if (seq.size() == 3) {
            TBSCertificate tbsCert = TBSCertificate.getInstance(seq.getObjectAt(0));
            Objects.requireNonNull(tbsCert);
            AlgorithmIdentifier sigAlgId = AlgorithmIdentifier.getInstance(seq.getObjectAt(1));
            Objects.requireNonNull(sigAlgId);

            ASN1BitString sig = ASN1BitString.getInstance(seq.getObjectAt(2));
            Objects.requireNonNull(sig);
        } else {
            throw new IllegalArgumentException("sequence wrong size for a certificate");
        }
    }

    public static Certificate getInstance(Object obj) {
        if (obj instanceof Certificate) {
            return (Certificate) obj;
        } else if (obj != null) {
            return new Certificate(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    public ASN1Primitive toASN1Primitive() {
        return seq;
    }
}
