package threads.lite.asn1;

import androidx.annotation.NonNull;

import java.io.IOException;

import threads.lite.asn1.util.Arrays;
import threads.lite.asn1.util.Strings;

public abstract class ASN1UTF8String extends ASN1Primitive implements ASN1String {
    final byte[] contents;

    ASN1UTF8String(String string) {
        this(Strings.toUTF8ByteArray(string));
    }

    ASN1UTF8String(byte[] contents) {
        this.contents = contents;
    }

    static ASN1UTF8String createPrimitive(byte[] contents) {
        return new DERUTF8String(contents);
    }

    public final String getString() {
        return Strings.fromUTF8ByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    public final int hashCode() {
        return Arrays.hashCode(contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1UTF8String)) {
            return false;
        }

        ASN1UTF8String that = (ASN1UTF8String) other;

        return Arrays.areEqual(this.contents, that.contents);
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.UTF8_STRING, contents);
    }
}
