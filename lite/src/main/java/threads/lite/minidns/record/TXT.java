/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.record;

import androidx.annotation.NonNull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A TXT record. Actually a binary blob containing extents, each of which is a one-byte count
 * followed by that many bytes of data, which can usually be interpreted as ASCII strings
 * but not always.
 */
public class TXT extends Data {

    private final byte[] blob;
    private transient String textCache;
    private transient List<String> characterStringsCache;

    public TXT(byte[] blob) {
        this.blob = blob;
    }

    public static TXT parse(DataInputStream dis, int length) throws IOException {
        byte[] blob = new byte[length];
        dis.readFully(blob);
        return new TXT(blob);
    }

    public String getText() {
        if (textCache == null) {
            StringBuilder sb = new StringBuilder();
            Iterator<String> it = getCharacterStrings().iterator();
            while (it.hasNext()) {
                sb.append(it.next());
                if (it.hasNext()) {
                    sb.append(" / ");
                }
            }
            textCache = sb.toString();
        }
        return textCache;
    }

    public List<String> getCharacterStrings() {
        if (characterStringsCache == null) {
            List<byte[]> extents = getExtents();
            List<String> characterStrings = new ArrayList<>(extents.size());
            for (byte[] extent : extents) {
                characterStrings.add(new String(extent, StandardCharsets.UTF_8));
            }

            characterStringsCache = Collections.unmodifiableList(characterStrings);
        }
        return characterStringsCache;
    }

    public List<byte[]> getExtents() {
        ArrayList<byte[]> extents = new ArrayList<>();
        int segLength;
        for (int used = 0; used < blob.length; used += segLength) {
            segLength = 0x00ff & blob[used];
            int end = ++used + segLength;
            byte[] extent = Arrays.copyOfRange(blob, used, end);
            extents.add(extent);
        }
        return extents;
    }

    @Override
    public void serialize(DataOutputStream dos) throws IOException {
        dos.write(blob);
    }


    @NonNull
    @Override
    public String toString() {
        return "\"" + getText() + "\"";
    }

}
