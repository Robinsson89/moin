package threads.moin.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;
import java.util.Objects;

import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;
import threads.moin.core.boxes.BoxesDatabase;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.core.mails.MailsDatabase;

public class MoinViewModel extends AndroidViewModel {
    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);
    @NonNull
    private final MutableLiveData<Long> note = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<Long> box = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<String> alias = new MutableLiveData<>("");
    @NonNull
    private final MutableLiveData<String> query = new MutableLiveData<>("");
    @NonNull
    private final MediatorLiveData<Void> mailsLiveDataMerger = new MediatorLiveData<>();
    @NonNull
    private final MediatorLiveData<Void> boxesLiveDataMerger = new MediatorLiveData<>();

    @NonNull
    private final MailsDatabase mailsDatabase;
    @NonNull
    private final BoxesDatabase boxesDatabase;

    public MoinViewModel(@NonNull Application application) {
        super(application);
        mailsDatabase = MAILS.getInstance(
                application.getApplicationContext()).getMailsDatabase();
        boxesDatabase = BOXES.getInstance(
                application.getApplicationContext()).getBoxesDatabase();
        mailsLiveDataMerger.addSource(box, value -> mailsLiveDataMerger.setValue(null));
        mailsLiveDataMerger.addSource(query, value -> mailsLiveDataMerger.setValue(null));

        boxesLiveDataMerger.addSource(query, value -> boxesLiveDataMerger.setValue(null));
    }

    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        getShowFab().postValue(show);
    }

    @NonNull
    public MutableLiveData<String> getQuery() {
        return query;
    }

    public void setQuery(@NonNull String query) {
        getQuery().postValue(query);
    }

    @NonNull
    private String getQueryValue() {
        MutableLiveData<String> queryData = getQuery();
        String query = queryData.getValue();
        String searchQuery = "";
        if (query != null) {
            searchQuery = query.trim();
        }
        if (!searchQuery.startsWith("%")) {
            searchQuery = "%" + searchQuery;
        }
        if (!searchQuery.endsWith("%")) {
            searchQuery = searchQuery + "%";
        }
        return searchQuery;
    }


    @NonNull
    public MutableLiveData<Long> getNote() {
        return note;
    }

    public void setNote(long note) {
        getNote().postValue(note);
    }


    public long getNoteValue() {
        Long value = getNote().getValue();
        return Objects.requireNonNull(value); // this should never be null
    }

    @NonNull
    public MutableLiveData<Long> getBox() {
        return box;
    }

    public void setBox(long box) {
        getBox().postValue(box);
    }


    @NonNull
    public MutableLiveData<String> getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        getAlias().postValue(alias);
    }

    public String getAliasValue() {
        String value = getAlias().getValue();
        return Objects.requireNonNull(value); // this should never be null
    }

    public long getBoxValue() {
        Long value = getBox().getValue();
        return Objects.requireNonNull(value); // this should never be null
    }


    @NonNull
    public LiveData<List<Mail>> getMails() {
        return Transformations.switchMap(
                mailsLiveDataMerger, input ->
                        mailsDatabase.mailDao().getLiveDataMails(getBoxValue(), getQueryValue()));
    }


    @NonNull
    public LiveData<List<Box>> getBoxes() {
        return Transformations.switchMap(
                boxesLiveDataMerger, input ->
                        boxesDatabase.boxDao().getLiveDataBoxesByQuery(getQueryValue()));
    }
}

