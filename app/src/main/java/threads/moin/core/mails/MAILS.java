package threads.moin.core.mails;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;
import java.util.UUID;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.moin.core.MimeType;
import threads.moin.core.boxes.Box;


public class MAILS {

    private static volatile MAILS INSTANCE = null;
    private final MailsDatabase mailsDatabase;


    private MAILS(MailsDatabase mailsDatabase) {
        this.mailsDatabase = mailsDatabase;
    }


    public static MAILS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (MAILS.class) {
                if (INSTANCE == null) {
                    MailsDatabase threadsDatabase = Room.databaseBuilder(context,
                                    MailsDatabase.class,
                                    MailsDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new MAILS(threadsDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public MailsDatabase getMailsDatabase() {
        return mailsDatabase;
    }

    public void setMailPublished(long idx) {
        getMailsDatabase().mailDao().setPublished(idx);
    }


    public void setMailReceived(long idx) {
        getMailsDatabase().mailDao().setReceived(idx);
    }

    public void setMailLocal(long idx, @NonNull Uri uri) {
        getMailsDatabase().mailDao().setLocal(idx, uri.toString());
    }


    @NonNull
    public List<Mail> getMails(@NonNull Box box) {

        return getMailsByBox(box.getIdx());
    }

    @NonNull
    private List<Mail> getMailsByBox(long box) {
        return getMailsDatabase().mailDao().getMailsByBox(box);
    }

    @NonNull
    public Mail createMessageMail(long box, @NonNull PeerId owner,
                                  @NonNull String text, long timestamp) {
        Mail mail = createMail(box, owner, Mail.Type.MESSAGE, null,
                MimeType.PLAIN_MIME_TYPE, timestamp);
        mail.setText(text);
        return mail;
    }

    @NonNull
    public Mail createFileMail(long box, @NonNull PeerId owner, long timestamp) {
        return Mail.createMail(box, owner, Mail.Type.FILE, timestamp);
    }

    @NonNull
    public Mail createAudioMail(long box, @NonNull PeerId owner,
                                @NonNull String mimeType, @NonNull String filename,
                                @NonNull Cid audio, long size, long timestamp) {

        Mail mail = createMail(box, owner, Mail.Type.AUDIO, audio, mimeType, timestamp);
        mail.setSize(size);
        mail.setFilename(filename);
        mail.setText(filename);
        return mail;
    }

    public long createInfo(long box, @NonNull PeerId owner,
                           @NonNull String info, long timestamp) {

        Mail mail = createMail(box, owner, Mail.Type.INFO,
                null, MimeType.PLAIN_MIME_TYPE, timestamp);
        mail.setText(info);
        mail.setSize(info.length());
        mail.setSeeding(true);
        mail.setPublished(true);
        mail.setReceived(true);
        return storeMail(mail);
    }

    public long storeMail(@NonNull Mail mail) {
        return getMailsDatabase().mailDao().insertMail(mail);
    }

    public void setSeeding(long idx, @NonNull Cid cid) {
        getMailsDatabase().mailDao().setSeeding(idx, cid);
    }

    public void unsetMailContent(long idx) {
        getMailsDatabase().mailDao().unsetContent(idx);
    }

    @Nullable
    public Mail getMailByIdx(long idx) {
        return getMailsDatabase().mailDao().getMailByIdx(idx);
    }

    private Mail createMail(long box, @NonNull PeerId owner, @NonNull Mail.Type type,
                            @Nullable Cid cid, @NonNull String mimeType,
                            long timestamp) {
        Mail mail = Mail.createMail(box, owner, type, timestamp);
        mail.setCid(cid);
        mail.setMimeType(mimeType);
        return mail;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isReferenced(@NonNull Cid cid) {

        int counter = getMailsDatabase().mailDao().references(cid);
        return counter > 0;
    }

    public void setSeeding(long idx) {
        getMailsDatabase().mailDao().setSeeding(idx);
    }

    public void setLeaching(long idx, UUID uuid) {
        getMailsDatabase().mailDao().setLeaching(idx, uuid.toString());
    }

    public void resetLeaching(long idx) {
        getMailsDatabase().mailDao().resetLeaching(idx);
    }

    public void setMailSize(long idx, long size) {
        getMailsDatabase().mailDao().setSize(idx, size);
    }


    public void removeMail(long idx) {
        getMailsDatabase().mailDao().removeMailByIdx(idx);
    }


    public List<Mail> getDeletedMails() {
        return getMailsDatabase().mailDao().getDeletedMails();
    }

    @NonNull
    public List<Mail> getNotifications(@NonNull PeerId owner, long box) {
        return getMailsDatabase().mailDao().getBoxNotifications(owner, box);
    }

}
