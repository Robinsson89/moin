package threads.moin.core.boxes;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Objects;

import threads.lite.cid.PeerId;
import threads.moin.core.MimeType;

@androidx.room.Entity
public class Box {

    @NonNull
    @ColumnInfo(name = "owner")
    @TypeConverters(PeerId.class)
    private final PeerId owner;
    @ColumnInfo(name = "date")
    private final long date;
    @PrimaryKey(autoGenerate = true)
    private long idx;
    @ColumnInfo(name = "number")
    private int number = 0;
    @ColumnInfo(name = "deleting")
    private boolean deleting;
    @NonNull
    @ColumnInfo(name = "mimeType")
    private String mimeType;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @NonNull
    @ColumnInfo(name = "content")
    private String content;


    Box(@NonNull PeerId owner, long date) {
        this.owner = owner;
        this.date = date;
        this.mimeType = MimeType.PLAIN_MIME_TYPE;
        this.deleting = false;
        this.name = "";
        this.content = "";
    }

    static Box createBox(@NonNull PeerId owner, long date) {
        return new Box(owner, date);
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(@NonNull String mimeType) {
        this.mimeType = mimeType;
    }

    @NonNull
    public PeerId getOwner() {
        return owner;
    }


    public boolean sameContent(@NonNull Box o) {

        if (this == o) return true;
        return number == o.getNumber() &&
                deleting == o.isDeleting() &&
                Objects.equals(name, o.getName()) &&
                Objects.equals(content, o.getContent()) &&
                Objects.equals(date, o.getDate());
    }

    public boolean areItemsTheSame(@NonNull Box box) {
        return idx == box.getIdx();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return getIdx() == box.getIdx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdx());
    }


    public int getNumber() {
        return number;
    }

    void setNumber(int number) {
        this.number = number;
    }

    public boolean isDeleting() {
        return deleting;
    }

    void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }


}
