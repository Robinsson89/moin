package threads.moin.core.boxes;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.PeerId;
import threads.moin.core.mails.Mail;


public class BOXES {

    private static volatile BOXES INSTANCE = null;
    private final BoxesDatabase boxesDatabase;

    private BOXES(BoxesDatabase boxesDatabase) {
        this.boxesDatabase = boxesDatabase;
    }

    public static BOXES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BOXES.class) {
                if (INSTANCE == null) {
                    BoxesDatabase boxesDatabase = Room.databaseBuilder(context,
                                    BoxesDatabase.class,
                                    BoxesDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new BOXES(boxesDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public BoxesDatabase getBoxesDatabase() {
        return boxesDatabase;
    }


    public void setBoxContent(long idx, @NonNull String content, @NonNull String mimeType) {

        getBoxesDatabase().boxDao().setContent(idx, content, mimeType, System.currentTimeMillis());
    }


    @NonNull
    public Box createBox(@NonNull PeerId owner) {

        return createBox(owner, System.currentTimeMillis());
    }

    public void resetBoxesNumber(long idx) {
        getBoxesDatabase().boxDao().resetNumber(idx);
    }

    public void incrementBoxesNumber(long idx) {
        getBoxesDatabase().boxDao().incrementNumber(idx);
    }

    public long storeBox(@NonNull Box box) {
        return getBoxesDatabase().boxDao().insertBox(box);
    }

    @NonNull
    public List<PeerId> getSwarm() {
        return getBoxesDatabase().boxDao().getSwarm();
    }

    @Nullable
    public Box getBoxByIdx(long idx) {
        return getBoxesDatabase().boxDao().getBoxByIdx(idx);
    }

    @Nullable
    public Box getBox(@NonNull Mail mail) {
        return getBoxByIdx(mail.getBox());
    }


    @NonNull
    private Box createBox(@NonNull PeerId owner, long date) {
        return Box.createBox(owner, date);
    }

    @Nullable
    public Box getBoxByOwner(@NonNull PeerId owner) {
        return getBoxesDatabase().boxDao().getBoxByOwner(owner);
    }

    public List<Box> getDeletedBoxes() {
        return getBoxesDatabase().boxDao().getDeletedBoxes();
    }

    public void removeBox(@NonNull Box box) {
        getBoxesDatabase().boxDao().removeBox(box);
    }

    public void setBoxesDeleting(long[] idxs) {
        getBoxesDatabase().boxDao().setBoxesDeleting(idxs);
    }

    public void setBoxName(long idx, @NonNull String name) {
        getBoxesDatabase().boxDao().setName(idx, name, System.currentTimeMillis());
    }


    public PeerId getBoxOwner(long box) {
        return getBoxesDatabase().boxDao().getOwner(box);
    }

    public void resetBoxesDeleting(long[] boxes) {
        getBoxesDatabase().boxDao().resetBoxesDeleting(boxes);
    }

}
