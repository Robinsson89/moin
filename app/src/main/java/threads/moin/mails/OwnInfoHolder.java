package threads.moin.mails;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import threads.moin.R;
import threads.moin.core.mails.Mail;

public class OwnInfoHolder extends MailHolder {
    final TextView message_body;

    public OwnInfoHolder(View v, Context context, MailAdapterListener listener) {
        super(v, context, listener);
        message_body = v.findViewById(R.id.message_body);
    }

    @Override
    public void bind(@NonNull Mail mail) {
        message_body.setText(mail.getText());
    }
}
