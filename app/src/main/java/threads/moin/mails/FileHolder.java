package threads.moin.mails;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import threads.moin.R;
import threads.moin.core.mails.Mail;
import threads.moin.services.MimeTypeService;

public class FileHolder extends MailHolder {

    final TextView user;
    final TextView size;
    final TextView message_body;
    final ImageView general_action;
    final ImageView image_view;

    final LinearProgressIndicator progress_bar;

    public FileHolder(View v, Context context, MailAdapterListener listener) {
        super(v, context, listener);


        general_action = v.findViewById(R.id.general_action);
        user = v.findViewById(R.id.peer);
        message_body = v.findViewById(R.id.message_body);
        image_view = v.findViewById(R.id.image_view);
        progress_bar = v.findViewById(R.id.progress_bar);
        size = v.findViewById(R.id.size);
    }

    @Override
    public void bind(@NonNull Mail mail) {
        user.setText(listener.getAlias());

        handleDate(mail);


        String mimeType = mail.getMimeType();

        int res = MimeTypeService.getMediaResource(mimeType);
        image_view.setImageResource(res);


        message_body.setText(mail.getFilename());
        String fileSize = getFileSize(mail);
        size.setText(fileSize);


        if (mail.isLeaching()) {
            progress_bar.setVisibility(View.VISIBLE);
            general_action.setImageResource(R.drawable.pause);
            general_action.setVisibility(View.VISIBLE);
            general_action.setOnClickListener((v) ->
                    listener.invokePauseAction(mail)
            );
        } else if (mail.isSeeding()) {
            progress_bar.setVisibility(View.INVISIBLE);
            general_action.setImageResource(R.drawable.copy_to);
            general_action.setVisibility(View.VISIBLE);
            general_action.setOnClickListener((v) ->
                    listener.invokeCopyTo(mail)
            );
        } else {
            progress_bar.setVisibility(View.INVISIBLE);
            general_action.setVisibility(View.VISIBLE);
            general_action.setImageResource(R.drawable.download);
            general_action.setOnClickListener((v) ->
                    listener.invokeDownloadAction(mail)
            );
        }


        if (!mimeType.isEmpty()) {
            view.setOnClickListener((v) -> listener.invokeAction(mail));
        }
    }
}