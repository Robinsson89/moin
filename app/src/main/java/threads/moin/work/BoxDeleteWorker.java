package threads.moin.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;

public class BoxDeleteWorker extends Worker {
    private static final String TAG = BoxDeleteWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public BoxDeleteWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(long... idxs) {

        Data.Builder data = new Data.Builder();
        data.putLongArray(Content.IDXS, idxs);

        return new OneTimeWorkRequest.Builder(BoxDeleteWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void delete(@NonNull Context context, long... idxs) {
        WorkManager.getInstance(context).enqueue(getWork(idxs));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        long[] idxs = getInputData().getLongArray(Content.IDXS);
        Objects.requireNonNull(idxs);
        try {
            BOXES boxes = BOXES.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());


            for (long idx : idxs) {
                Box box = boxes.getBoxByIdx(idx);
                if (box != null) {
                    if (box.isDeleting()) {
                        api.leave(box.getOwner());
                        api.removeBox(box);
                    }
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

}
