package threads.moin.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Connection;
import threads.lite.core.Progress;
import threads.lite.utils.TimeoutCancellable;
import threads.moin.InitApplication;
import threads.moin.LogUtils;
import threads.moin.MainActivity;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;
import threads.moin.core.events.EVENTS;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.provider.Provider;


public class DownloadFileWorker extends Worker {
    private static final String TAG = DownloadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public DownloadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .addTag(DownloadFileWorker.TAG)
                .setInputData(data.build())
                .build();
    }


    public static void download(@NonNull Context context, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                "DFW" + idx, ExistingWorkPolicy.KEEP, getWork(idx));
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        MAILS mails = MAILS.getInstance(getApplicationContext());
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            BOXES boxes = BOXES.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());
            EVENTS events = EVENTS.getInstance(getApplicationContext());
            Provider provider = Provider.getInstance(getApplicationContext());


            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);

            Cid cid = mail.getCid();
            Objects.requireNonNull(cid);

            mails.setLeaching(idx, getId());

            Box box = boxes.getBox(mail);
            Objects.requireNonNull(box);


            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            String title = getApplicationContext().getString(R.string.download, mail.getFilename());
            Notification.Builder builder = new Notification.Builder(
                    getApplicationContext(), InitApplication.MESSAGE_CHANNEL_ID);

            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true)
                    .setOngoing(true);


            boolean doProgress = mail.getSize() > API.PROGRESS_LIMIT_BYTES;

            int notificationId = getId().hashCode();
            if (doProgress) {
                Notification notification = builder.build();
                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));
            }


            Connection conn = api.connect(mail.getOwner(), new TimeoutCancellable(120));

            if (conn == null) {
                events.warning(getApplicationContext().getString(
                        R.string.no_connection_to, box.getName()));
                return Result.success();
            }


            File file = provider.createDataFile(cid);

            ipfs.fetchToFile(api.getSession(), file, cid, new Progress() {
                @Override
                public boolean isCancelled() {
                    return isStopped();
                }

                public void setProgress(int progress) {
                    builder.setSubText("" + progress + "%")
                            .setProgress(100, progress, false);
                    notificationManager.notify(notificationId, builder.build());
                }

                @Override
                public boolean doProgress() {
                    return doProgress;
                }

            });

            Uri uri = Provider.getDataUri(getApplicationContext(), file);
            Objects.requireNonNull(uri);
            mails.setMailLocal(idx, uri);
            ipfs.removeBlocks(api.getSession(), cid); // remove file

            if (mail.getSize() != file.length()) {
                mails.setMailSize(idx, file.length());
            }

            mails.setSeeding(idx);

            long ident = mail.getIdent();

            if (ident > 0) {
                api.received(conn, ident);
            }

            notificationManager.cancel(notificationId);


        } catch (Throwable throwable) {
            mails.resetLeaching(idx);
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }


}
