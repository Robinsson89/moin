package threads.moin.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.work.WorkManager;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import threads.lite.core.Server;
import threads.moin.InitApplication;
import threads.moin.LogUtils;
import threads.moin.MainActivity;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.work.SwarmWorker;

public class DaemonService extends Service {

    private static final String TAG = DaemonService.class.getSimpleName();
    private static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    private static final String ACTION_START_SERVICE = "ACTION_START_SERVICE";
    private final Timer timer = new Timer();

    public static void start(@NonNull Context context) {
        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.setAction(ACTION_START_SERVICE);
            ContextCompat.startForegroundService(context, intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    SwarmWorker.connect(getApplicationContext());
                }

                @Override
                public void onLost(Network network) {

                }
            });

            if (threads.lite.cid.Network.isNetworkConnected(getApplicationContext())) {
                SwarmWorker.connect(getApplicationContext());
            }

            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    SwarmWorker.connect(getApplicationContext());
                }
            }, 15 * 60 * 1000, 15 * 60 * 1000); // 15 min for both
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(ACTION_START_SERVICE)) {
            buildNotification();
        } else if (intent.getAction().equals(ACTION_STOP_SERVICE)) {
            try {
                stopForeground(STOP_FOREGROUND_REMOVE);
            } finally {
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    private void buildNotification() {
        Notification.Builder builder = new Notification.Builder(
                getApplicationContext(), InitApplication.DAEMON_CHANNEL_ID);

        Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
        int viewID = (int) System.currentTimeMillis();
        PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                viewID, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT |
                        PendingIntent.FLAG_IMMUTABLE);

        Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
        stopIntent.setAction(ACTION_STOP_SERVICE);
        int stopID = (int) System.currentTimeMillis();
        PendingIntent stopPendingIntent = PendingIntent.getService(
                getApplicationContext(), stopID, stopIntent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause),
                getApplicationContext().getString(R.string.shutdown),
                stopPendingIntent).build();

        builder.setSmallIcon(R.drawable.access_point_network);
        builder.setContentTitle(getString(R.string.service_is_running));
        builder.setContentIntent(viewIntent);
        builder.addAction(action);
        builder.setUsesChronometer(true);
        builder.setCategory(Notification.CATEGORY_SERVICE);
        builder.setOnlyAlertOnce(true);
        Notification notification = builder.build();
        startForeground(TAG.hashCode(), notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            timer.cancel();

            // closing app
            WorkManager.getInstance(getApplicationContext()).cancelAllWork();


            API api = API.getInstance(getApplicationContext());
            Server server = api.getServer();
            server.shutdown();

            removeFromResents();
            System.exit(0);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void removeFromResents() {
        try {
            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                List<ActivityManager.AppTask> tasks = am.getAppTasks();
                if (tasks != null && tasks.size() > 0) {
                    tasks.get(0).setExcludeFromRecents(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            registerNetworkCallback();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


}
